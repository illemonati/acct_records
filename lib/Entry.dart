//import 'package:json_annotation/json_annotation.dart';
//
//part 'entry.g.dart';

//@JsonSerializable(nullable: false)
class Entry {
  int number = -1;
  String title = "";
  String username = "";
  String password = "";
  String notes = "";

  Entry(this.number, this.title, this.username, this.password, this.notes);

  Entry.fromRaw(Map value) {
    number = value['number'];
    title = value['title'];
    username = value['username'];
    password = value['password'];
    notes = value['notes'];
  }

  Map toJson() {
    return {"number": number, "title": title, "username": username, "password": password, "notes": notes};
  }
}
