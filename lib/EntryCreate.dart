import 'package:flutter/material.dart';
import 'package:acct_records/io.dart';
import 'dart:async';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'dart:convert';
import 'package:acct_records/Entry.dart';

class EntryCreate extends StatefulWidget {
  EntryCreate(this.ctx, this.currentEntryList);

  final BuildContext ctx;
  final currentEntryList;

  @override
  EntryCreateState createState() => new EntryCreateState(currentEntryList);
}

class EntryCreateState extends State<EntryCreate> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  List<String> _colors = <String>['', 'red', 'green', 'blue', 'orange'];
  String _color = '';
  final List<Entry> currentEntryList;

  EntryCreateState(this.currentEntryList);

  Future<File> get _localFile async {
    final directory = await getApplicationDocumentsDirectory();
    final path = directory.path;
    return File('$path/list.json');
  }

  Future<File> _writeList(List<Entry> list) async {
    final file = await _localFile;
// Write the file
    return file.writeAsString(jsonEncode(list));
  }

  void _addToList(String title, String name, String passwd, String notes, List<Entry> entryList) async {
    int entryNumber = 0;
    if (entryList.length > 0) {
      entryNumber = entryList[entryList.length - 1].number + 1;
    }
    entryList.add(new Entry(entryNumber, title, name, passwd, notes));
    final file = await _writeList(entryList);
  }

  void _addToListAndReturn(String title, String name, String passwd, String notes, List<Entry> entryList) async {
    _addToList(title, name, passwd, notes, entryList);
    print(jsonEncode(entryList));
    Navigator.pop(context);
  }

  final TextEditingController title = new TextEditingController();
  final TextEditingController username = new TextEditingController();
  final TextEditingController password = new TextEditingController();
  final TextEditingController notes = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("New Entry"),
      ),
      body: new SafeArea(
          top: false,
          bottom: false,
          child: new Form(
              key: _formKey,
              autovalidate: true,
              child: new ListView(
                padding: const EdgeInsets.symmetric(horizontal: 16.0),
                children: <Widget>[
                  new TextFormField(
                    decoration: const InputDecoration(
                      hintText: 'Enter the title',
                      labelText: 'Title',
                    ),
                    controller: title,
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      hintText: 'Enter the username',
                      labelText: 'Username',
                    ),
                    controller: username,
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      hintText: 'Enter the password',
                      labelText: 'Password',
                    ),
                    controller: password,
                  ),
                  new TextFormField(
                    decoration: const InputDecoration(
                      hintText: 'Enter notes',
                      labelText: 'Notes',
                    ),
                    controller: notes,
                  ),
                  new Container(
                      padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                      child: new RaisedButton(
                          child: const Text('Save'),
                          onPressed: () =>
                              _addToListAndReturn(
                                  title.text,
                                  username.text,
                                  password.text,
                                  notes.text,
                                  this.currentEntryList))),
                ],
              ))),
    );
  }
}
