import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:acct_records/Entry.dart';
import 'package:path_provider/path_provider.dart';

Future<File> get _localFile async {
  final directory = await getApplicationDocumentsDirectory();
  final path = directory.path;
  return File('$path/list.json');
}

Future<List<Entry>> _readList() async {
  try {
    final file = await _localFile;
// Read the file
    String contents = await file.readAsString();
    List<Entry> list = jsonDecode(contents);
    return list;
  } catch (e) {
// If encountering an error, return 0
    return <Entry>[];
  }
}

Future<File> _writeList(List<Entry> list) async {
  final file = await _localFile;
// Write the file
  return file.writeAsString(jsonEncode(list));
}
