import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:acct_records/Entry.dart';
import 'package:acct_records/EntryCreate.dart';
import 'package:acct_records/EntryDetailScreen.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
//    String wordPair = WordPair.random().asPascalCase.toString();
    return MaterialApp(
      title: 'Account Records',
      home: Accts(),
    );
  }
}

class Accts extends StatefulWidget {
  @override
  AcctState createState() => new AcctState();
}

class AcctState extends State<Accts> {
  var entryList = <Entry>[];

//  final entryList = _readList().then(onValue(List<Entry> l) => return l;);
  final textStyle = const TextStyle(fontSize: 18);

  @override
  void initState() {
    _readList().then((result) {
      // If we need to rebuild the widget with the resulting data,
      // make sure to use `setState`
      setState(() {
        entryList = result;
      });
    });
  }

  Future<File> get _localFile async {
    final directory = await getApplicationDocumentsDirectory();
    final path = directory.path;
    return File('$path/list.json');
  }

  Future<List<Entry>> _readList() async {
    try {
      final file = await _localFile;
// Read the file
      String contents = await file.readAsString();
//      print(contents);
//      var dson = new Dartson.JSON();
      List<dynamic> dl = jsonDecode(contents);
      List<Entry> list = <Entry>[];
      for (dynamic d in dl) {
//        print(d);
//        print(d["title"]);
        list.add(new Entry.fromRaw(d));
      }
      return list;
    } catch (e) {
//      print(e);
      return <Entry>[];
    }
  }

  Widget _buildList(BuildContext ctx) {
    return ListView.builder(
      itemCount: entryList.length * 2 + 1,
      itemBuilder: (ctx, i) {
        if (i.isOdd) {
          return Divider();
        }
        final realIndex = i ~/ 2;
        if (realIndex == entryList.length) {
          return ListTile(
            title: Text("Add Account"),
            onTap: () => Navigator.push(ctx, MaterialPageRoute(builder: (BuildContext ctx) => EntryCreate(ctx, this.entryList))),
          );
        }

        return _buildRow(entryList[realIndex], ctx);
      },
    );
  }

  Widget _buildRow(Entry ent, BuildContext ctx) {
    return ListTile(
      title: Text(
        ent.title,
        style: this.textStyle,
      ),
      onTap: () => Navigator.push(ctx, MaterialPageRoute(builder: (BuildContext ctx) => EntryDetailScreen(ctx, entryList, ent))),
    );
  }

  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Account Records'),
      ),
      body: _buildList(ctx),
    );
  }
}
