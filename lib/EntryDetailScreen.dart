import 'package:acct_records/Entry.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'dart:convert';

class EntryDetailScreen extends StatelessWidget {
  List<Entry> entryList;
  BuildContext ctx;
  Entry ent;

  EntryDetailScreen(this.ctx, this.entryList, this.ent);

  Future<File> get _localFile async {
    final directory = await getApplicationDocumentsDirectory();
    final path = directory.path;
    return File('$path/list.json');
  }

  Future<File> _writeList(List<Entry> list) async {
    final file = await _localFile;
// Write the file
    return file.writeAsString(jsonEncode(list));
  }

  void _deleteFromListAndReturn() async {
    entryList.removeWhere((Entry entry) => entry.number == this.ent.number);
    await _writeList(entryList);
    Navigator.pop(this.ctx);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text(ent.title)),
        body: ListView(
          children: <Widget>[
            ListTile(
              leading: Text("Username: "),
              title: Text(ent.username),
            ),
            ListTile(
              leading: Text("Password: "),
              title: Text(ent.password),
            ),
            ListTile(
              leading: Text("Notes: "),
              title: Text(ent.notes),
            ),
            new Container(
              padding: const EdgeInsets.only(left: 20.0, top: 20.0),
              child: new RaisedButton(
                child: const Text('Delete'),
                onPressed: () =>
                _deleteFromListAndReturn()
              )
            ),
          ],
        ));
  }
}
